package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
)

var (
	infoLogger *log.Logger
)

type request struct {
	Method  string              `json:"method"`
	Path    string              `json:"path"`
	Query   string              `json:"query,omitempty"`
	Headers map[string][]string `json:"headers"`
	Body    string              `json:"body,omitempty"`
}

func init() {
	infoLogger = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
}

func handler(w http.ResponseWriter, r *http.Request) {
	req := request{Method: r.Method, Path: r.URL.Path, Query: r.URL.RawQuery}
	req.Headers = make(map[string][]string)
	for name, values := range r.Header {
		req.Headers[name] = values
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(req)
}

func main() {
	port := flag.Int("port", 8080, "The port to bind to")
	http.HandleFunc("/", handler)
	infoLogger.Printf("Starting server on port %d....\n", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}
