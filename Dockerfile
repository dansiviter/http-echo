FROM golang:alpine AS builder
RUN apk update && apk add --no-cache build-base
WORKDIR $GOPATH/src/
COPY . .
RUN go get -d -v
RUN go build -ldflags "-linkmode external -extldflags -static" -a echo.go

FROM scratch
COPY --from=builder /go/src/echo /go/bin/echo
EXPOSE 8080
ENTRYPOINT ["/go/bin/echo"]
